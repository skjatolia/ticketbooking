Steps to run the project

On terminal run below commands

1. npm i
2. npm run dev
3. click the url to open booking page
4. enter number of seats to book. If booked it returns seats number and shows coach seat booked layout

**Working Link:** https://ticket-booking-unstop-sunil-jatolias-projects.vercel.app/
Code: zip file attached to mail.
language: Typescript + React
Deployed at Vercel

**Database structure can be viewed at:**
https://dbdiagram.io/d/Train_Ticket_Booking_Database_Diagram-65729ef556d8064ca0a025d9

**QUERIES**

1. **Train seats layout**
   Select Seat_Row_No, Seat_Column_No from TrainSeatDetails
   where Train_Id = 'dd81472b-c4b6-43b5-99bd-c78eeafe409f'

2. **Booked Seats layout**
   Select Seat_Row_No, Seat_Column_No from TrainSeatDetails
   where Seat_Id in (
   Select ts.Seat_Id from Tickets as t join TicketSeatsDetail as ts
   on t.Ticket_Id = ts.Ticket_Id
   where t.Train_Id = 'dd81472b-c4b6-43b5-99bd-c78eeafe409f' and ts.Ticket_Status = 'CONFIRMED'
   )

3. **Total Seats Count**
   Select Total_Seats_In_Coach from Trains
   where Train_Id = 'dd81472b-c4b6-43b5-99bd-c78eeafe409f'

4. **Booked Seats Count**
   Select count(ts.Seat_Id) as 'BookedSeatsCount' from Tickets as t join TicketSeatsDetail as ts
   on t.Ticket_Id = ts.Ticket_Id
   where t.Train_Id = 'dd81472b-c4b6-43b5-99bd-c78eeafe409f' and ts.Ticket_Status = 'CONFIRMED'

5. **Available Seats Count**
   Select ((
   Select Total_Seats_In_Coach from Trains
   where Train_Id = 'dd81472b-c4b6-43b5-99bd-c78eeafe409f'
   ) -
   (
   Select count(ts.Seat_Id) as 'BookedSeatsCount' from Tickets as t join TicketSeatsDetail as ts
   on t.Ticket_Id = ts.Ticket_Id
   where t.Train_Id = 'dd81472b-c4b6-43b5-99bd-c78eeafe409f' and ts.Ticket_Status = 'CONFIRMED'
   ))
   as AvailableSeatCount
