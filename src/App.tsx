import { useState } from "react";
import BookingForm from "./TicketBooking/BookingForm";
import SeatReservation from "./TicketBooking/SeatReservation";

const App = () => {
  const [seatsData, setseatsData] = useState<number[][]>([]);
  const [bookedSeats, setBookedSeats] = useState<number[]>([]);
  const [availableSeats, setavailableSeats] = useState<number>(-1);
  let i = 0,
    j = 1000,
    seatno = 1;
  {
    console.log(availableSeats);
  }
  return (
    <>
      <div className="mb-5">
        <BookingForm
          onSubmit={(data) => {
            const newData = SeatReservation(data.seat);
            setseatsData(newData.seatlayout.filter((a) => a[1] !== -100));
            setBookedSeats(newData.bookedSeats.filter((a) => a !== -100));
            setavailableSeats(newData.availbleSeat);
          }}
        ></BookingForm>
      </div>
      {availableSeats > 0 && bookedSeats.length === 0 && (
        <div className="mb-4">
          <p>
            <b>Not Enough Seats Available.</b>
          </p>
        </div>
      )}
      {bookedSeats.length > 0 && (
        <div className="mb-4">
          <p>
            <b>Booked Seats Number:</b>
          </p>
          {bookedSeats.map((b) => (
            <span className="btn ">{b}</span>
          ))}
        </div>
      )}
      {seatsData.length > 0 && (
        <div>
          <p>
            <b>
              Available Seats : {availableSeats} <br /> Coach Seat Booking
              Layout
            </b>{" "}
            (1 = booked | 0 = empty | () = seat number):
          </p>
          <table className="table table-bordered">
            <tbody>
              {seatsData.map((row) => (
                <tr key={"row" + i++}>
                  {row.map((cell) => (
                    <td key={"col" + j++}>
                      {cell}{" "}
                      <span className="btn right text-primary">
                        ({seatno++})
                      </span>
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
    </>
  );
};

export default App;
