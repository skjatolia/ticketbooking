// Initializing details will be fetched from database
const TotalSeats: number = 80;
const noOfSeatsInRow: number = 7;
const noOfSeatsInLastRow: number = TotalSeats % noOfSeatsInRow;
const noOfColumns: number = (TotalSeats - noOfSeatsInLastRow) / noOfSeatsInRow;

// Coach seating arrangements will be fetched from database and stored in the seats matrix
const seats: number[][] = new Array(noOfColumns)
  .fill(0)
  .map(() => new Array(noOfSeatsInRow).fill(0));

if (noOfSeatsInLastRow > 0) {
  let x = Array(noOfSeatsInLastRow).fill(0);
  seats.push(x);
}

const getCoachSeatingArrangement = () => {
  // Coach seating arrangements will be fetched from database and stored in the seats matrix
  // seats matrix initialized.
};

const getBookedSeatsDetails = () => {
  // Booked seats details will be fetch from database and confirmed seats will be marked with 1 in seats matrix
  // seats matrix marked with confirmed seats.
};

const getSeatNumbers = (): number[][] => {
  let i = 1;
  const seatNumbers: number[][] = seats.map((row) => row.map(() => i++));
  return seatNumbers;
};

const SeatReservation = (
  noOfSeatToBook: number
): { seatlayout: number[][]; bookedSeats: number[]; availbleSeat: number } => {
  // console.log("TotalSeats", TotalSeats);
  // console.log("noOfSeatsInRow", noOfSeatsInRow);
  // console.log("noOfSeatsInLastRow", noOfSeatsInLastRow);
  // console.log("noOfColumns", seats.length);
  // console.log("noOfSeatToBook", noOfSeatToBook);
  // console.log("availableSeats", availableSeats());
  // console.log("seats.length", seats.length);

  getCoachSeatingArrangement();
  getBookedSeatsDetails();

  let bookedSeatNumbers: number[] = [];

  if (noOfSeatToBook > availableSeats()) {
    console.log(
      "Seats to book is greater tha Available Seats: ",
      availableSeats()
    );
    console.log(seats);
    return {
      seatlayout: seats,
      bookedSeats: [],
      availbleSeat: availableSeats(),
    };
  }

  if (noOfSeatToBook <= 0 || noOfSeatToBook > 7) {
    // console.log("Minimum 1 and maximum 7 seats can be booked at a time");
    // console.log(seats);
    return {
      seatlayout: seats,
      bookedSeats: [],
      availbleSeat: availableSeats(),
    };
  }

  // Priority : Check if seats are in one row then book them.
  let remainingSeatsToBook = noOfSeatToBook;
  let row = checkSeatAvailableInOneRow(remainingSeatsToBook);
  if (row >= 0) {
    bookedSeatNumbers = bookSeats(remainingSeatsToBook, -100);
    remainingSeatsToBook = 0;
    console.log("bookedSeatNumbers", bookedSeatNumbers);
    return {
      seatlayout: seats,
      bookedSeats: bookedSeatNumbers,
      availbleSeat: availableSeats(),
    };
  }

  // If seats not in one row then
  let seatsInEachRow = getAvailableSeatsInEachRow();
  let adjacentRowIndex = getAdjacentRowIndexs(
    seatsInEachRow,
    remainingSeatsToBook
  );

  bookedSeatNumbers = [];
  if (adjacentRowIndex.length > 0) {
    for (let r = 0; r < adjacentRowIndex.length; r++) {
      let index = adjacentRowIndex[r];
      if (remainingSeatsToBook === 0) {
        break;
      }
      if (remainingSeatsToBook < seatsInEachRow[index]) {
        bookedSeatNumbers = [
          ...bookedSeatNumbers,
          ...bookSeats(remainingSeatsToBook, index),
        ];
        remainingSeatsToBook = 0;
      } else {
        remainingSeatsToBook = remainingSeatsToBook - seatsInEachRow[index];
        bookedSeatNumbers = [
          ...bookedSeatNumbers,
          ...bookSeats(seatsInEachRow[index], index),
        ];
      }
    }
  }

  console.log("bookedSeatNumbers", bookedSeatNumbers);
  return {
    seatlayout: seats,
    bookedSeats: bookedSeatNumbers,
    availbleSeat: availableSeats(),
  };
};

// Find total available seats in coach
// This can be fetched from database directly
const availableSeats = () => {
  let count = 0;
  seats.forEach((seat) =>
    seat.forEach((s) => {
      if (s === 0) count++;
    })
  );

  return count;
};

// Method to check if all seats are available in one row. If yes return row index otherwise return -1
const checkSeatAvailableInOneRow = (noOfSeatToBook: number) => {
  for (let i = seats.length - 1; i >= 0; i--) {
    let availableSeatInRow = 0;
    let seat = seats[i];
    seat.forEach((s) => {
      if (s === 0) availableSeatInRow++;
    });

    if (noOfSeatToBook <= availableSeatInRow) {
      return i;
    }
  }
  return -1;
};

// Find seats available in each row
const getAvailableSeatsInEachRow = () => {
  let i = 0;
  let seatsInEachRow: number[] = new Array(seats.length);
  seats.forEach((seat) => {
    let seatCount = 0;
    seat.forEach((s) => {
      if (s === 0) {
        seatCount++;
      }
    });

    seatsInEachRow[i] = seatCount;
    i++;
  });

  return seatsInEachRow;
};

// Find adjacent row indexs for total number of seats to book
const getAdjacentRowIndexs = (
  seatsInEachRow: number[],
  seatsToBook: number
): number[] => {
  var i,
    j,
    k,
    sum = 0;

  // Find all combinations of adjacent rows
  const combinations = [];
  for (j = 0; j < seatsInEachRow.length; j++) {
    for (i = 0; i < seatsInEachRow.length - j - 1; i++) {
      sum = 0;
      let indexs = [];
      for (k = i; k <= i + 1 + j && k < seatsInEachRow.length; k++) {
        sum += seatsInEachRow[k];
        indexs.push(k);
      }

      const c = { sum: sum, indexs: indexs };
      if (seatsToBook <= sum) {
        combinations.push(c);
      }
    }
    if (sum >= noOfSeatsInRow || seatsToBook <= sum) {
      break;
    }
  }

  if (combinations.length > 0) {
    combinations.sort((a, b) => {
      if (a.sum !== b.sum) {
        return a.indexs.length - b.indexs.length; // Sort by the seat count with less number of adjasant rows
      } else {
        // sort by seat count
        return a.sum - b.sum;
      }
    });
    return combinations[0].indexs;
  }
  return [];
};

// Book Seats at given row index
const bookSeats = (
  noOfSeatToBook: number,
  seatBookingRow: number
): number[] => {
  let remainingSeatsToBook = noOfSeatToBook;
  let bookedSeatNumbers: number[] = [];
  const SeatNumbersArray = getSeatNumbers();
  if (seatBookingRow < 0)
    seatBookingRow = checkSeatAvailableInOneRow(noOfSeatToBook);
  console.log("seatBookingRow", seatBookingRow);
  if (seatBookingRow >= 0) {
    for (let j = 0; j < seats[seatBookingRow].length; j++) {
      if (seats[seatBookingRow][j] === 0 && remainingSeatsToBook > 0) {
        seats[seatBookingRow][j] = 1;
        bookedSeatNumbers.push(SeatNumbersArray[seatBookingRow][j]);
        remainingSeatsToBook--;
      }
    }
    console.log("");
    console.log(noOfSeatToBook, "Booking Confirmed");
    console.log("Seats available", availableSeats());

    console.log(seats);
  }
  return bookedSeatNumbers;
};

export default SeatReservation;
