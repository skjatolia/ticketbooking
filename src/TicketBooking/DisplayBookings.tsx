interface Props {
  data: number[][];
}

const DisplayBookings = ({ data }: Props) => {
  let i = 0,
    j = 1000;

  return (
    <table className="table table-bordered">
      {/* <thead>
          <tr>
            {data[0].map((header) => (
              <th key={header}>{header}</th>
            ))}
          </tr>
        </thead> */}
      <tbody>
        {data.map((row) => (
          <tr key={"row" + i++}>
            {row.map((cell) => (
              <td key={"col" + j++}>{cell}</td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default DisplayBookings;
