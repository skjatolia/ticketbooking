import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";

const schema = z.object({
  seat: z
    .number({ invalid_type_error: "Number required" })
    .min(1, { message: "Minimum 1 seat can be booked" })
    .max(7, { message: "Maximum 7 seats can be booked at a time" }),
});

type BookingFormData = z.infer<typeof schema>;

interface Props {
  onSubmit: (data: BookingFormData) => void;
}

const BookingForm = ({ onSubmit }: Props) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<BookingFormData>({ resolver: zodResolver(schema) });

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="mb-4">
          <label htmlFor="seat" className="form-label">
            <b>Enter Number of Seats to Book</b>
          </label>
          <input
            {...register("seat", { valueAsNumber: true })}
            id="seat"
            type="number"
            className="form-control"
          />
          {errors.seat && <p className="text-danger">{errors.seat.message}</p>}
        </div>
        <button className="btn btn-primary">Submit</button>
      </form>
    </>
  );
};

export default BookingForm;
